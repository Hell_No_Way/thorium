package com.example.thorium

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cell_info_tb")
class CellInformation(
        @PrimaryKey(autoGenerate = true) val id: Int? = null,
        val TYPE: String,
        val CID: String,
        val MCC: String,
        val MNC: String,
        val PLMN: String,
        val LONG: Double,
        val ALT: Double,
        val STRENGTH: String,
        val GSM_RSSI: String,
        val LAC: String,
        val TAC: String,
        val TIME: Long,
        val LATENCY: Long,
        val CONTENT_LATENCY: Long,
        val ARFCN: String

)