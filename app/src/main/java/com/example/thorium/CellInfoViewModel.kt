package com.example.thorium

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CellInfoViewModel(application: Application) : AndroidViewModel(application) {

    private val storage: CellInfoRepo
    val infoList: LiveData<List<CellInformation>>
    init {
        val infoDao = CellInfoDatabase.getDatabase(application, viewModelScope).cellInfoObject()
        storage = CellInfoRepo(infoDao)
        infoList = storage.dataList


    }

    fun insert(cellInfo: CellInformation) = viewModelScope.launch(Dispatchers.IO) {
        storage.insert(cellInfo)
    }
}
