package com.example.thorium

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class InfoListAdapter internal constructor(
    context: Context
) : RecyclerView.Adapter<InfoListAdapter.WordViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var info_list = emptyList<CellInformation>() // this will cache the copy of infgo_list

    inner class WordViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val type_IV: TextView = itemView.findViewById(R.id.type)
        val cid_IV : TextView = itemView.findViewById(R.id.cid)
        val mnc_IV: TextView = itemView.findViewById(R.id.mnc)
        val mcc_IV: TextView = itemView.findViewById(R.id.mcc)
        val plmn_IV : TextView = itemView.findViewById(R.id.plmn)
        val altitude_IV: TextView = itemView.findViewById(R.id.altitude)
        val longitude_IV: TextView = itemView.findViewById(R.id.longitude)
        val strength_IV: TextView = itemView.findViewById(R.id.strength)
        val gsm_rssi_IV: TextView = itemView.findViewById(R.id.gsm_rssi)
        val lac_IV: TextView = itemView.findViewById(R.id.lac)
        val tac_IV: TextView = itemView.findViewById(R.id.tac)
        val time_IV: TextView = itemView.findViewById(R.id.time)
        val latency_IV: TextView = itemView.findViewById(R.id.latency)
        val content_latency_IV: TextView = itemView.findViewById(R.id.content_latency)
        val arfcn_IV: TextView = itemView.findViewById(R.id.arfcn)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return WordViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        val current = info_list[position]
        holder.cid_IV.text = "Cid => ${current.CID}"
        holder.type_IV.text = "Type => ${current.TYPE}"
        holder.mnc_IV.text = "Mnc => ${current.MNC}"
        holder.mcc_IV.text = "Mcc => ${current.MCC}"
        holder.plmn_IV.text = "PLMN => ${current.PLMN}"
        holder.altitude_IV.text = "Altitude => ${current.ALT}"
        holder.longitude_IV.text = "Longitude => ${current.LONG}"
        holder.strength_IV.text = "Strength => ${current.STRENGTH}"
        holder.gsm_rssi_IV.text = "RSSI => ${current.GSM_RSSI} "
        holder.lac_IV.text = "Lac => ${current.LAC}"
        holder.tac_IV.text = "Tac => ${current.TAC}"
        holder.time_IV.text = "Time => ${current.TIME}"
        holder.latency_IV.text = "Latency => ${current.LATENCY}"
        holder.content_latency_IV.text = "Content Latency => ${current.CONTENT_LATENCY}"
        holder.arfcn_IV.text = "ARFCN => ${current.ARFCN}"
        try {
            val parent: ViewGroup = holder.gsm_rssi_IV.parent as ViewGroup

            if (current.TYPE != "GSM")
            {
                parent.removeView(holder.gsm_rssi_IV)
            }
            if (current.TYPE != "LTE")
            {
                parent.removeView(holder.lac_IV)
            }
            if (current.TYPE == "LTE")
            {
                parent.removeView(holder.tac_IV)
            }
        }
        catch (e: TypeCastException)
        {

        }

    }

    internal fun setdata(data: List<CellInformation>) {
        this.info_list = data
        notifyDataSetChanged()
    }

    override fun getItemCount() = info_list.size
}