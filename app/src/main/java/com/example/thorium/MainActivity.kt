package com.example.thorium

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.telephony.*
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_map.*
import java.io.IOException
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {
    private lateinit var infoViewModel: CellInfoViewModel
    private var current_location: Location? = null
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    var latency : Long = 0
    var content_latency :Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        val tm = this.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = InfoListAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        val startButton = findViewById<Button>(R.id.start)


        infoViewModel = ViewModelProvider(this).get(CellInfoViewModel::class.java)
        infoViewModel.infoList.observe(this, Observer { data ->
            // Update the cached copy of the words in the adapter.
            data?.let { adapter.setdata(it) }
        })

        //start button listener
        startButton.setOnClickListener {
            Dexter.withContext(this)
                .withPermissions(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.ACCESS_NETWORK_STATE
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        val mainHandler = Handler(Looper.getMainLooper())
                        mainHandler.post(object : Runnable {
                            @RequiresApi(Build.VERSION_CODES.M)
                            override fun run() {
                                processInitiator(tm)
                                mainHandler.postDelayed(this, 10000)
                            }
                        })

                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: List<PermissionRequest?>?,
                        token: PermissionToken?
                    ) {
                    }
                }).check()
        }


    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.map, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.map_header -> {
                showMap()
                return true
            }
        }
        return super.onOptionsItemSelected(item)

    }


    @RequiresApi(Build.VERSION_CODES.P)
    fun processInitiator(tm: TelephonyManager){
        var TYPE: String = ""
        var CID : String = ""
        var MCC: String = ""
        var MNC: String = ""
        var PLMN: String = ""
        var STRENGTH: String = ""
        var GSM_RSSI: String = ""
        var LAC: String = ""
        var TAC: String = ""
        var ARFCN : String = ""
        val infoList = tm.allCellInfo
        if (infoList.size == 0){
            Toast.makeText(this@MainActivity, "No Signal", Toast.LENGTH_SHORT).show()
        }

        try {
            val cellInfo = tm.allCellInfo[0]
            if (cellInfo is CellInfoGsm) {
                val cellSignalStrengthGsm: CellSignalStrengthGsm = cellInfo.cellSignalStrength
                val cellIdentityGsm: CellIdentityGsm = cellInfo.cellIdentity
                TYPE = "GSM"
                CID = cellIdentityGsm.cid.toString()
                LAC = cellIdentityGsm.lac.toString()
                MCC = cellIdentityGsm.mccString.toString()
                MNC = cellIdentityGsm.mncString.toString()
                PLMN = MCC + MNC
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    ARFCN = cellIdentityGsm.arfcn.toString()
                }
                //rac = cellIdentityGsm.rac.toString()
                STRENGTH = cellSignalStrengthGsm.dbm.toString()
                GSM_RSSI = cellSignalStrengthGsm.asuLevel.toString()
            }
            if (cellInfo is CellInfoWcdma) {
                val cellSignalStrengthWcdma: CellSignalStrengthWcdma = cellInfo.cellSignalStrength
                val cellIdentityWcdma: CellIdentityWcdma = cellInfo.cellIdentity
                TYPE = "UMTS"
                CID = cellIdentityWcdma.cid.toString()
                MCC = cellIdentityWcdma.mcc.toString()
                MNC = cellIdentityWcdma.mnc.toString()
                PLMN = MCC + MNC
                STRENGTH = cellSignalStrengthWcdma.dbm.toString()
                LAC = cellIdentityWcdma.lac.toString()
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    ARFCN = cellIdentityWcdma.uarfcn.toString()
                }
            }
            if (cellInfo is CellInfoLte) {
                val cellSignalStrengthLte: CellSignalStrengthLte = cellInfo.cellSignalStrength
                val cellIdentityLte: CellIdentityLte = cellInfo.cellIdentity
                TYPE = "LTE"
                CID = cellIdentityLte.ci.toString()
                MCC = cellIdentityLte.mccString.toString()
                MNC = cellIdentityLte.mncString.toString()
                PLMN = MCC + MNC
                TAC = cellIdentityLte.tac.toString()
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    ARFCN = cellIdentityLte.earfcn.toString()
                }
                STRENGTH = cellSignalStrengthLte.dbm.toString()

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    cellSignalStrengthLte.rsrp.toString()
                }
            }
            if (cellInfo is CellInfoCdma) {
                val cellSignalStrengthCdma: CellSignalStrengthCdma = cellInfo.cellSignalStrength
                TYPE = "UMTS"
                STRENGTH = cellSignalStrengthCdma.dbm.toString()


            }
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            var netInfo: NetworkInfo? = cm.activeNetworkInfo

            if (netInfo != null && netInfo.isConnected) {
                val nc: NetworkCapabilities = cm.getNetworkCapabilities(cm.activeNetwork)
            }

            latency = latencycal("49.51.171.79") // Valorant Bahrain Server
            content_latency = contlatencycal("www.iust.ir")
        }
        catch (e: IndexOutOfBoundsException) {
            Toast.makeText(this@MainActivity, "No Signal", Toast.LENGTH_SHORT).show()
        }
        finally {
            requestNewLocationData()
            if (current_location != null)
            {
                val longStr = String.format("%.3f",current_location!!.longitude)
                val altStr = String.format("%.3f",current_location!!.altitude)
                val dataInformation = CellInformation(
                        TYPE = TYPE,
                        CID = CID,
                        MCC = MCC,
                        MNC = MNC,
                        PLMN = PLMN,
                        LONG = longStr.toDouble(),
                        ALT = altStr.toDouble(),
                        STRENGTH = STRENGTH,
                        GSM_RSSI = GSM_RSSI,
                        LAC = LAC,
                        TAC = TAC,
                        TIME = System.currentTimeMillis(),
                        LATENCY = latency,
                        CONTENT_LATENCY = content_latency,
                        ARFCN = ARFCN
                        )
                infoViewModel.insert(dataInformation)
            }
        }

    }
    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            var mLastLocation: Location = locationResult.lastLocation
            current_location = mLastLocation
        }
    }

    fun showMap() {
        val mapIntent = Intent(this, MapActivity::class.java)
        startActivity(mapIntent)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun latencycal(addr: String) :Long {
        val runtime = Runtime.getRuntime()
        var ping : Long =999999999
        try {
            var a : Long = (System.currentTimeMillis() %100000)
            val ipSet = runtime.exec("/system/bin/ping -c 1 "+ addr)
            val mExitValue  = ipSet.waitFor(2, TimeUnit.SECONDS)
            ping = if (mExitValue ){
                var b : Long = (System.currentTimeMillis() %100000)
                if(b<=a){
                    (100000 - a) + b
                }else{
                    b - a
                }
            }else{
                999999999
            }

        } catch (ignore: InterruptedException) {
            ignore.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return ping
    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun contlatencycal(addr: String) :Long {
        val runtime = Runtime.getRuntime()
        var pingContent : Long = 999999999
        try {
            var a : Long = (System.currentTimeMillis() % 100000)
            val ipSet = runtime.exec("/system/bin/ping -c 1 "+ addr)
            val mExitValue  = ipSet.waitFor(2,TimeUnit.SECONDS)
            pingContent = if (mExitValue ){
                var b : Long = (System.currentTimeMillis() % 100000)
                if (b<=a) {
                    (100000 - a) + b
                }else{
                    b - a
                }
            }else{
                999999999
            }
        } catch (ignore: InterruptedException) {
            ignore.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return pingContent
    }


}
