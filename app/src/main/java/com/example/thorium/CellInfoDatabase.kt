package com.example.thorium

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import kotlinx.coroutines.CoroutineScope

@Database(entities = [CellInformation::class], version = 1, exportSchema = false)
public abstract class CellInfoDatabase : RoomDatabase(){
    abstract fun cellInfoObject(): CellInfoObject
    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var Instance: CellInfoDatabase? = null
        fun getDatabase(context: Context, scope: CoroutineScope): CellInfoDatabase {
            val tmpInst = Instance
            if (tmpInst != null) {
                return tmpInst
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    CellInfoDatabase::class.java,
                    "cell_info_db"
                ).allowMainThreadQueries().build()
                Instance = instance
                return instance
            }
        }
    }
}