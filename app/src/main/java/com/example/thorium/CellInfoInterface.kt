package com.example.thorium

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface CellInfoObject {
    @Query("SELECT * from cell_info_tb ORDER BY TIME DESC")
    fun getData(): LiveData<List<CellInformation>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(cell_info: CellInformation)
}