package com.example.thorium

import androidx.lifecycle.LiveData

class CellInfoRepo(private val cellInfoObject: CellInfoObject) {

    val dataList: LiveData<List<CellInformation>> = cellInfoObject.getData()

    suspend fun insert(cellInfo: CellInformation) {
        cellInfoObject.insert(cellInfo)
    }
}