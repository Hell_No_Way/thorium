package com.example.thorium

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

class MapActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var infoViewModel: CellInfoViewModel
    private lateinit var map: GoogleMap
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        infoViewModel = ViewModelProvider(this).get(CellInfoViewModel::class.java)
        infoViewModel.infoList.observe(this, Observer { data ->
            // Update the list of markers
            data?.let { updateMarkers(it) }
        })


    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        map.setMyLocationEnabled(true)
        map.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
            override fun getInfoWindow(arg0: Marker): View? {
                return null
            }

            override fun getInfoContents(marker: Marker): View {
                val info = LinearLayout(applicationContext)
                info.orientation = LinearLayout.VERTICAL
                val title = TextView(applicationContext)
                title.setTextColor(Color.CYAN)
                title.gravity = Gravity.CENTER
                title.setTypeface(null, Typeface.BOLD)
                title.text = marker.title
                val snippet = TextView(applicationContext)
                snippet.setTextColor(Color.GRAY)
                snippet.text = marker.snippet
                info.addView(title)
                info.addView(snippet)
                return info
            }
        })
    }

    private fun getMarkerColor(cellInfo: CellInformation) : Float {
        var color: Float
        when(cellInfo.TYPE) {
            "GSM" -> {
                color = when {
                    cellInfo.STRENGTH.toInt() >= -70 -> BitmapDescriptorFactory.HUE_RED
                    cellInfo.STRENGTH.toInt() >= -85 -> BitmapDescriptorFactory.HUE_CYAN
                    cellInfo.STRENGTH.toInt() >= -100 -> BitmapDescriptorFactory.HUE_YELLOW
                    cellInfo.STRENGTH.toInt() >= -110 -> BitmapDescriptorFactory.HUE_ORANGE
                    else -> BitmapDescriptorFactory.HUE_BLUE
                }
            }
            "UMTS" -> {
                color = when {
                    cellInfo.STRENGTH.toInt() >= -70 -> BitmapDescriptorFactory.HUE_RED
                    cellInfo.STRENGTH.toInt() >= -85 -> BitmapDescriptorFactory.HUE_CYAN
                    cellInfo.STRENGTH.toInt() >= -100 -> BitmapDescriptorFactory.HUE_YELLOW
                    cellInfo.STRENGTH.toInt() >= -110 -> BitmapDescriptorFactory.HUE_ORANGE
                    else -> BitmapDescriptorFactory.HUE_BLUE
                }
            }
            "LTE" -> {
                color = when {
                    cellInfo.STRENGTH.toInt() >= -80 -> BitmapDescriptorFactory.HUE_RED
                    cellInfo.STRENGTH.toInt() >= -90 -> BitmapDescriptorFactory.HUE_CYAN
                    cellInfo.STRENGTH.toInt() >= -100 -> BitmapDescriptorFactory.HUE_YELLOW
                    cellInfo.STRENGTH.toInt() >= -110 -> BitmapDescriptorFactory.HUE_ORANGE
                    else -> BitmapDescriptorFactory.HUE_BLUE
                }

            }


            else -> color = BitmapDescriptorFactory.HUE_MAGENTA
        }
        return color
    }

    private fun updateMarkers(infoList: List<CellInformation>) {
        for (cellInfo in infoList) {
            val pos = LatLng(cellInfo.ALT, cellInfo.LONG)
            val color = getMarkerColor(cellInfo)

            val snip = "Strength: " + cellInfo.STRENGTH + "\n" +
                    "PLMN: " + cellInfo.MCC + cellInfo.MNC + "\n" +
                    "latency: " + cellInfo.LATENCY + " ms\n"
            val marker = map.addMarker(MarkerOptions().icon(
                BitmapDescriptorFactory.defaultMarker(color)).position(
                pos).title(cellInfo.TYPE).snippet(snip))
        }
    }
}